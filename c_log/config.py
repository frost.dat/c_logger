#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Config(object):
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDCOLOR = '\033[0m'
